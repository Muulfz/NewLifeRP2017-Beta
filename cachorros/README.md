# K9 Script
This is a K9 script that uses the AI to handles the k9 functions.

## Requirements
WarMenu - https://forum.fivem.net/t/release-0-9-7-warmenu-lua-gui-framework/41249

## Installing Instructions (Make sure that WarMenu is installed before the K9)
1. Download K9 Script From my github page.
2. Drag the k9-master folder into your resources.(you can rename the k9-master to (k9) or whatever you want.
3. Add your k9 resource name to your server.cfg (start k9-master).

## Configuration

### K9_Config.PedRestricted = true or false
Sets if the script is restricted to certain ped models

### K9_Config.VehicleRestricted = true or false
Sets if the k9 is restricted to only enter certain vehicles.

### K9_Config.GodmodeDog = true or false
Sets the dog in godmode to give him the upper hand in chasing your suspects.

## Controls
1. [G] - Toggles the dog to follow or stay.
2. [AIM + G] - Tells the dog to attack the ped you are aiming at(must use a lethal weapon at longer distances).
3. [LeftALT + Z] - Opens the K9 options menu. (Handles K9 animations, K9 Spawning / Deleting, And the vehicle toggle)
