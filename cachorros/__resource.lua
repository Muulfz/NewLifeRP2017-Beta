--[[
    Scripted by: Xander Harrison [X. Cross]
--]]

resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

--[[ CONFIG ]]--
client_script 'cl_k9_config.lua'
client_script '@warmenu/warmenu.lua'
--]]

client_script 'cl_k9.lua'