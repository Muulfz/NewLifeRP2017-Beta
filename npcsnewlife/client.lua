-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

local generalLoaded = false
local PlayingAnim = false

-------------------------------------------------------------------------------------------------
----------------------------------- YOU CAN EDIT THIS LINES -------------------------------------
-------------------------------------------------------------------------------------------------

local ShopClerk = {
  -- ID: id of NPC | name: Name of Blip | BlipID: Icone of Blip | VoiceName: NPC Talk When near it | Ambiance: Ambiance of Shop | Weapon: Hash of Weapon | modelHash: Model | X: Position x | Y: Position Y | Z: Position Z | heading: Where Npc look
	{id = 1, name = "Shop", VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "mp_m_shopkeep_01", x = -2511.16479492188, y = 3616.90478515625, z = 13.6422147750854, heading = 245.000457763672}, 
	{id = 2, name = "Shop", VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "mp_m_shopkeep_01", x = 24.392505645752, y = -1345.41369628906, z = 29.4970207214355, heading = 264.900115966797},
	{id = 3, name = "Shop", VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "mp_m_shopkeep_01", x = -47.3110542297363, y = -1758.62475585938, z = 29.4209995269775, heading = 48.1558074951172},
	{id = 4, name = "Ammunation", VoiceName = "GENERIC_HI", Ambiance = "AMMUCITY", Weapon = 0x1D073A89, modelHash = "s_m_y_ammucity_01", x = 841.843566894531, y = -1035.70556640625, z = 28.1948642730713, heading = 3.31448912620544},
	{id = 5, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_stripper_01", x = 112.35987854004, y = -1287.2749023438, z = 28.458688735962, heading = 100.0}, 
	{id = 6, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_stripper_01", x = 112.3050994873, y = -1285.611328125, z = 28.458683013916, heading = 100.0}, 
	{id = 7, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_stripper_01", x = 113.71821594238, y = -1288.1514892578, z = 28.458683013916, heading = 100.0}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_stripper_01", x = 103.51350402832, y = -1292.2377929688, z = 29.258701324463, heading = 100.0}, 
	{id = 9, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "ig_kerrymcintosh", x = 321.63137817383, y = -236.35029602051, z = 54.006484985352, heading = 146.73}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_hooker_01", x = -119.71990203857, y = -210.12989807129, z = 44.706069946289, heading = 166.78}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_hooker_02", x = -549.97991943359, y = 271.43923950195, z = 82.958862304688, heading = 2150.72}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_hooker_03", x = -380.00466918945, y = 224.35928344727, z = 84.12353515625, heading = 18.4}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_stripper_01", x = -532.8583984375, y = -1190.0793457031, z = 19.331274032593, heading = 20.7},
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "a_f_y_juggalo_01", x = 1053.2504882813, y = 2300.7700195313, z = 29.258701324463, heading = 103.2}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "s_f_y_bartender_01", x = 122.56119537354, y = -1305.7827148438, z = 29.227533340454, heading = 142.7}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "a_f_m_beach_01", x = 498.02267456055, y = 707.04339599609, z = 25.036270141602, heading = 343.9}, 
	{id = 8, name = "Stripper", VoiceName = "Sex_Generic_Fem", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "ig_kerrymcintosh", x = 696.69573974609, y = -1005.5230712891, z = 22.90154838562, heading = 79.7}, 
	{id = 8, name = "Stripper", VoiceName = "SHOP_GREET", Ambiance = "AMMUCITY", Weapon = 0xA2719263, modelHash = "a_m_m_tranvest01", x = 236.58255004883, y = 333.30047607422, z = 105.5286026001, heading = 293.3}, 
}



-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

-- Blip For NPC
Citizen.CreateThread(function()
	for k,v in pairs(ShopClerk)do
		local blip = AddBlipForCoord(v.x, v.y, v.z)
		SetBlipSprite(blip, v.BlipID)
		SetBlipScale(blip, 0.8)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(v.name)
		EndTextCommandSetBlipName(blip)
	end
end)

-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

-- Spawn NPC
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
	
	if (not generalLoaded) then
	  
	  for i=1, #ShopClerk do
        RequestModel(GetHashKey(ShopClerk[i].modelHash))
        while not HasModelLoaded(GetHashKey(ShopClerk[i].modelHash)) do
          Wait(1)
        end
		
        ShopClerk[i].id = CreatePed(2, ShopClerk[i].modelHash, ShopClerk[i].x, ShopClerk[i].y, ShopClerk[i].z, ShopClerk[i].heading, true, true)
        SetPedFleeAttributes(ShopClerk[i].id, 0, 0)
		SetAmbientVoiceName(ShopClerk[i].id, ShopClerk[i].Ambiance)
		SetPedDropsWeaponsWhenDead(ShopClerk[i].id, false)
		SetPedDiesWhenInjured(ShopClerk[i].id, false)
		GiveWeaponToPed(ShopClerk[i].id, ShopClerk[i].Weapon, 2800, false, true)
		
      end
      generalLoaded = true
		
    end
	
  end
end)

-------------------------------------------------------------------------------------------------
---------------------------------- DON'T EDIT THESES LINES --------------------------------------
-------------------------------------------------------------------------------------------------

-- Action when player Near NPC (or not)
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
		RequestAnimDict("random@shop_gunstore")
		while (not HasAnimDictLoaded("random@shop_gunstore")) do 
			Citizen.Wait(0) 
		end
		
		for i=1, #ShopClerk do
			distance = GetDistanceBetweenCoords(ShopClerk[i].x, ShopClerk[i].y, ShopClerk[i].z, GetEntityCoords(GetPlayerPed(-1)))
			if distance < 4 and PlayingAnim ~= true then
				TaskPlayAnim(ShopClerk[i].id,"random@shop_gunstore","_greeting", 1.0, -1.0, 4000, 0, 1, true, true, true)
				PlayAmbientSpeech1(ShopClerk[i].id, ShopClerk[i].VoiceName, "SPEECH_PARAMS_FORCE", 1)
				PlayingAnim = true
				Citizen.Wait(4000)
				if PlayingAnim == true then
					TaskPlayAnim(ShopClerk[i].id,"random@shop_gunstore","_idle_b", 1.0, -1.0, -1, 0, 1, true, true, true)
					Citizen.Wait(40000)
				end
			else
				--don't touch this
				--TaskPlayAnim(ShopClerk[i].id,"random@shop_gunstore","_idle", 1.0, -1.0, -1, 0, 1, true, true, true)
			end
			if distance > 5.5 and distance < 6 then
				PlayingAnim = false
			end


		end
  end
end)

