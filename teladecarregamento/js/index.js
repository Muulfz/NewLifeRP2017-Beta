$( document ).ready(function() {
  setTimeout(loadDatSkweenie, 2000);
});

function loadDatSkweenie() {
  var banner = ["&nbsp", "N" ,"e", "w", "&nbsp","L", "i", "f", "e", "&nbsp", "R", "P", "&nbsp"]
  var rules = ["Não fazer spam no chat.", 
               "Respeitar os admin e os outros jogadores.", 
               "Não mate os jogadores sem razão (RDM).", 
               "Seguir o role-play.", 
               "Uso obrigatório do Microfone!",
               "DP e Hospital são Safe-Zone.",
               "Proibido VDM.",
               "Respeite as regras."
              ]
  var fadeTime = 500
  var fadeTime2 = 500
  $(".infohold").fadeIn(900)
  $(".steamid").show(900)
  $(banner).each(function( i ) {
    var tCount = Number(i)
    fadeTime = fadeTime + 200
    $(".banner p:nth-child("+tCount+")").hide()
    $( ".banner" ).append( "<p>"+banner[tCount]+"</p>" );
    $(".banner p:nth-child("+tCount+")").fadeIn(fadeTime)
  })
  $(rules).each(function( i ) {
    var rCount = Number(i) 
    fadeTime2 = fadeTime2 + 300
    $(".block .text:nth-child("+rCount+")").hide()
    $( ".block:nth-child(1)" ).append( "<p class='text'>"+rules[rCount]+"</p>" )
    $(".block .text:nth-child("+rCount+")").show(fadeTime2)
  })
}