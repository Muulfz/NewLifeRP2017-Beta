lang = "br"

MenuCanBeOpen = true ---- Set it to false if you don't want that the menu can be open
MenuCanBeOpenByAllPlayers = false ---- If you set it to true, all players will access to the menu

MenuWhiteList = { --- List of poeple who can open the menu
	"steam:110000104ee544d", --Muulfz
	"steam:110000116a7248e",  --Shadow
	"steam:110000106fd4613",  -- Heishiro
	"steam:110000114db56ce",  -- First
	"steam:11000010d2a22c2",  -- First
	"steam:110000114fab219",  -- First
}






settings =  {}

settings["en"] = {
	automatic = "Set the height automaticly",
	deplacementPrecision = "Set precision",
	height = "Set object height",
	delCurrent = "Delete the current object",
	cancel = "Cancel",
	confirm = "Place the object",
	camPrecision = "Set the precision of the camera",
	menuInstruction1 = "Keys : \n~g~Arrows~w~\n ~g~Q/E~w~\n~g~Page UP/Down~w~",
	menuInstruction2 = "Keys : ~g~Q/E~w~\n ~g~W/S~w~\n~g~Page UP/Down~w~",
	successfull = "You successfully added this object !",
	deleteSuccess = "You successfully deleted the object !",
	spawnByName = "Spawn object by name",
	searchObject = "Search objects by tags",
	deleteObject = "Deleted placed object"
}

settings["br"] = {
	automatic = "Ajuste a altura automaticamente",
	deplacementPrecision = "Definir precisão",
	height = "Definir altura do objeto",
	delCurrent = "Exclua o objeto atual",
	cancel = "Cancelar",
	confirm = "Coloque o objeto",
	camPrecision = "Defina a precisão da câmera",
	menuInstruction1 = "Teclas : \n~g~Arrows~w~\n ~g~Q/E~w~\n~g~Page UP/Down~w~",
	menuInstruction2 = "Teclas : ~g~Q/E~w~\n ~g~W/S~w~\n~g~Page UP/Down~w~",
	successfull = "Você adicionou este objeto com sucesso!",
	deleteSuccess = "Você excluiu o objeto com sucesso!",
	spawnByName = "Spawn objeto por nome",
	searchObject = "Pesquisar objetos por tags",
	deleteObject = "Objeto excluído"
}


settings["fr"] = {
	automatic = "Définir la hauteur automatiquement",
	deplacementPrecision = "Définir la précision",
	height = "Définir la hauteur de l'objet",
	delCurrent = "Supprimer l'actuel objet",
	cancel = "Annuler",
	confirm = "Placer l'objet",
	camPrecision = "Définir la précision de la caméra",
	menuInstruction1 = "Touches : \n~g~Flêches~w~\n ~g~A/E~w~\n~g~Page UP/Down~w~",
	menuInstruction2 = "Touches : \n~g~A/E~w~\n ~g~Z/S~w~\n ~g~Page UP/Down~w~",
	successfull = "Vous venez de placer cet objet !",
	deleteSuccess = "Vous venez de supprimer cet objet !",
	spawnByName = "Spawn un objet par le nom",
	searchObject = "chercher un objet par un mot clé",
	deleteObject = "Supprimer un objet placé"
}