resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

-- Example custom radios
supersede_radio "RADIO_02_POP" { url = "http://167.114.218.6:8000/newlife.ogg", volume = 0.5 }
supersede_radio "RADIO_01_CLASS_ROCK" { url = "http://relay-chi.gameowls.com:8000/all.ogg", volume = 0.2 }
supersede_radio "RADIO_03_HIPHOP_NEW" { url = "http://www.shinsen-radio.org:8000/shinsen-radio.64.ogg", volume = 0.2 }

files {
	"index.html"
}

ui_page "index.html"

client_scripts {
	"data.js",
	"client.js"
}
